import * as React from 'react';
import { StyleSheet, Text, View, Image, SafeAreaView } from 'react-native';
import {WebView} from 'react-native-webview';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';

function Home(){
  return(
    <WebView 
      style={{flex:1,marginTop:30,marginLeft:'12%'}}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      startInLoadingState={true}
      source={{uri:'http://200.202.244.146:8080/default_index_html'}}
    />
  );}

function Calendar(){
  return(
    <WebView 
      style={{flex:1,marginTop:30,marginLeft:'12%'}}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      startInLoadingState={true}
      source={{uri:"https://calendar.google.com/calendar/embed?title=Agenda Câmara Municipal de Poços de Caldas&showNav=0&showDate=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0&mode=AGENDA&height=100&wkst=1&bgcolor=%23ffffff&src=camarapocos%40gmail.com&color=%2328754E&ctz=America%2FSao_Paulo"}}
    />
  );}

  function Holerite(){
    return(
      <WebView 
        style={{flex:1,marginTop:30,marginLeft:'12%'}}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
        source={{uri:'https://minhafolha.cloud.betha.com.br/'}}
      />
    );}

  function Telefones(){
    return(
      <WebView 
        style={{flex:1,marginTop:30,marginLeft:'12%'}}
        source={{html: require('./assets/telefones').listaTelefones()}}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
      />
    );}  

    function Contatos(){
      return(
        <WebView 
          style={{flex:1,marginTop:30,marginLeft:'12%'}}
          source={{html: require('./assets/contatos').listaContatos()}}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={true}
        />
      );}  

const Drawer =createDrawerNavigator();

  function MyDrawer(){
    return(
      <Drawer.Navigator 
        openByDefault='true'
        drawerType='slide'
        drawerContentOptions={{
        activeTintColor: 'white',
        inactiveTintColor: 'white'
        }}
        drawerStyle={{
          backgroundColor:'#59864b',
          width:'35%',
          marginTop:30,
          marginLeft:'12%',
        }}
        >
        <Drawer.Screen
          name='Início'
          component={Home}
          options={{drawerIcon: config => <Icon style={{alignSelf: "center",position: "absolute",right: 5}} color='white' size={25} name={'home'}/>}}
        />
        <Drawer.Screen
          name='Calendário'
          component={Calendar}
          options={{drawerIcon: config => <Icon style={{alignSelf: "center",position: "absolute",right: 5}} color='white' size={25} name={'calendar'}/>}}
        />
        <Drawer.Screen
          name='Holerite'
          component={Holerite}
          options={{drawerIcon: config => <Icon style={{alignSelf: "center",position: "absolute",right:9}} color='white' size={25} name={'dollar'}/>}}
        />
        <Drawer.Screen
          name='Telefones'
          component={Telefones}
          options={{drawerIcon: config => <Icon style={{alignSelf: "center",position: "absolute",right:9}} color='white' size={25} name={'phone'}/>}}
        />
        <Drawer.Screen
          name='Contatos'
          component={Contatos}
          options={{drawerIcon: config => <Icon style={{alignSelf: "center",position: "absolute",right:9}} color='white' size={25} name={'user'}/>}}
        />
      </Drawer.Navigator>
    );}

export default function App() {
  return (
    <NavigationContainer>
      <MyDrawer></MyDrawer>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  WebViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 30,
  },
});