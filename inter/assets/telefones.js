export const listaTelefones = () => String.raw `
<!doctype html>
<html lang="en">
<head>
  <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
  
  <style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
  }
  
  td,th {
    padding: 5px;
  }

  th {
    text-align: center;
    background-color: #59864b;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }

  a:link {
    color: #59864b;
    text-decoration: none;
  }
  
  a:visited {
    color: #59864b;
    text-decoration: none;
  }
  
  a:hover {
    color: #000;
    text-decoration: none;
  }
  
  a:active {
    color: #59864b;
    text-decoration: none;
  }
  </style>

</head>
<body>
<table>
<colgroup>
  <col>
  <col style="width:25%">
</colgroup> 
<tr> 
<th colspan="2">Assessoria Administrativa</th>
</tr> 
<tr> 
<td>Kleber (Assessor Administrativo)</td> 
<td><a href="tel:3729-3817">3729-3817</a></td></td> 
</tr> 
<tr> 
<td>Ludmila</td> 
<td><a href="tel:3729-3854">3729-3854</a></td> 
</tr> 
<tr> 
<td>Marcusé (Pregoeiro e Chefe de Patrimônio)</td> 
<td><a href="tel:3729-3816">3729-3816</a></td>
</tr> 
<tr> 
<td>Roselene</td>
<td><a href="tel:3729-3861">3729-3861</a></td>
</tr> 
<tr> 
<td>Jaílton (Segurança)</td> 
<td><a href="tel:3729-3860">3729-3860</a></td>
</tr> 
<tr> 
<td>Serviços Gerais</td> 
<td><a href="tel:3729-3811">3729-3811</a></td>
</tr> 
<tr> 
<td>Serviços Gerais Anexo</td> 
<td><a href="tel:3729-3871">3729-3871</a></td>
</tr> 
<tr> 
<td>Cabine de som do Plenário</td> 
<td><a href="tel:3729-3844">3729-3844</a></td>
</tr> 
<tr> 
<th colspan="2">Assessoria Financeira</th> 
</tr> 
<tr> 
<td>Ricardo (Assessor Financeiro)</td>
<td><a href="tel:3729-3859">3729-3859</a></td>
</tr> 
<tr> 
<td>Luis Augusto (Chefe de Tesouraria e RH)</td>
<td><a href="tel:3729-3814">3729-3814</a></td>
</tr> 
<tr> 
<td>Cleiton</td>
<td><a href="tel:3729-3874">3729-3874</a></td>
</tr> 
<tr> 
<th colspan="2">Assessoria de Imprensa e Comunicações</th>
</tr> 
<tr> 
<td>Ana Amélia (Assessora de Imprensa e Comunicações)</td>
<td><a href="tel:3729-3818">3729-3818</a></td>
</tr> 
<tr> 
<td>Taís (Chefe do Setor de Comunicações e Cerimonial)</td>
<td><a href="tel:3729-3877">3729-3877</a></td>
</tr> 
<tr> 
<td>Deivide (Setor de Comunicações e Cerimonial)</td>
<td><a href="tel:3729-3845">3729-3845</a></td>
</tr> 
<tr> 
<td>Maria de Lourdes (Setor de Comunicações e Cerimonial)</td>
<td><a href="tel:3729-3845">3729-3845</a></td>
</tr> 
<tr> 
<th colspan="2">Assessoria Jurídica</th>
</tr> 
<tr> 
<td>Cristiano (Assessor Jurídico)</td>
<td><a href="tel:3729-3812">3729-3812</a></td>
</tr>  
<tr> 
<td>Milena</td>
<td><a href="tel:3729-3805">3729-3805</a></td>
</tr> 
<tr> 
<th colspan="2">Assessoria Técnica de Informática</th>
</tr> 
<tr> 
<td>Douglas (Assessor Técnico de Informática)</td>
<td><a href="tel:3729-3808">3729-3808</a></td>
</tr> 
<tr> 
<td>Marcos</td>
<td><a href="tel:3729-3840">3729-3840</a></td>
</tr> 
<tr> 
<td>Matheus</td>
<td><a href="tel:3729-3840">3729-3840</a></td>
</tr> 
<tr> 
<td>Apoio Sede</td>
<td><a href="tel:3729-3810">3729-3810</a></td>
</tr> 
<tr> 
<th colspan="2">Assessoria Técnica Legislativa</th>
</tr> 
<tr> 
<td>Filipe (Assessor Técnico Legislativo)</td>
<td><a href="tel:3729-3807">3729-3807</a></td>
</tr> 
<tr> 
<td>Vanessa (Chefe de Expediente)</td>
<td><a href="tel:3729-3806">3729-3806</a></td>
</tr> 
<tr> 
<td>Ana Paula</td>
<td><a href="tel:3729-3851">3729-3851</a></td>
</tr> 
<tr> 
<td>Antônio</td>
<td><a href="tel:3729-3849">3729-3849</a></td>
</tr> 
<tr> 
<td>Leonardo</td>
<td><a href="tel:3729-3848">3729-3848</a></td>
</tr> 
<tr> 
<td>Patrícia</td>
<td><a href="tel:3729-3802">3729-3802</a></td>
</tr> 
<tr> 
<td>Rodrigo</td>
<td><a href="tel:3729-3803">3729-3803</a></td>
</tr> 
<tr> 
<th colspan="2">Controle Interno</th>
</tr> 
<tr>
<td>Paulo Roberto</td>
<td><a href="tel:3729-3876">3729-3876</a></td>
</tr> 
<tr> 
<th colspan="2">Escola do Legislativo</th>
</tr> 
<tr> 
<td>Taís (Diretora)</td>
<td><a href="tel:3729-3877">3729-3877</a></td>
</tr> 
<tr> 
<th colspan="2">Vereadores e Assessores</th>
</tr> 
<tr> 
<td>Claudiney Donizetti Marques</td>
<td><a href="tel:3729-3837">3729-3837</a></td>
</tr> 
<tr> 
<td>Aliff</td>
<td><a href="tel:3729-3822">3729-3822</a></td>
</tr> 
<tr> 
<td>Diney Lenon de Paulo</td>
<td><a href="tel:3729-3853">3729-3853</a></td>
</tr> 
<tr> 
<td>Rosimeire</td>
<td><a href="tel:3729-3852">3729-3852</a></td>
</tr> 
<tr> 
<td>Douglas Eduardo de Souza</td>
<td><a href="tel:3729-3820">3729-3820</a></td>
</tr> 
<tr> 
<td>Isadora</td>
<td><a href="tel:3729-3821">3729-3821</a></td>
</tr> 
<tr> 
<td>Flavio Togni de Lima e Silva</td>
<td><a href="tel:3729-3831">3729-3831</a></td>
</tr> 
<tr> 
<td>Aline Fallaci</td>
<td><a href="tel:3729-3830">3729-3830</a></td>
</tr> 
<tr> 
<td>Kleber Golcalves da Silva</td>
<td><a href="tel:3729-3827">3729-3827</a></td>
</tr> 
<tr> 
<td>Karina</td>
<td><a href="tel:3729-3839">3729-3839</a></td>
</tr> 
<tr> 
<td>Lucas Carvalho de Arruda</td>
<td><a href="tel:3729-3835">3729-3835</a></td>
</tr> 
<tr> 
<td>Rafael Faria</td>
<td><a href="tel:3729-3836">3729-3836</a></td>
</tr> 
<tr> 
<td>Luzia Teixeira Martins</td>
<td><a href="tel:3729-3832">3729-3832</a></td>
</tr> 
<tr> 
<td>Michelle</td>
<td><a href="tel:3729-3824">3729-3824</a></td>
</tr> 
<tr> 
<td>Marcelo Heitor da Silva</td>
<td><a href="tel:3729-3825">3729-3825</a></td>
</tr> 
<tr> 
<td>Rafael</td>
<td><a href="tel:3729-3826">3729-3826</a></td>
</tr> 
<tr> 
<td>Regina Maria Cioffi Batagini</td>
<td><a href="tel:3729-3847">3729-3847</a></td>
</tr> 
<tr> 
<td>Aline Pereira</td>
<td><a href="tel:3729-3843">3729-3843</a></td>
</tr> 
<tr> 
<td>Ricardo Sabino dos Santos</td>
<td><a href="tel:3729-3856">3729-3856</a></td>
</tr> 
<tr> 
<td>Paulo</td>
<td><a href="tel:3729-3855">3729-3855</a></td>
</tr> 
<tr> 
<td>Sebastiao Roberto dos Santos</td>
<td><a href="tel:3729-3841">3729-3841</a></td>
</tr> 
<tr> 
<td>Christopher</td>
<td><a href="tel:3729-3822">3729-3822</a></td> 
</tr> 
<tr> 
<td>Silvio Rogerio Carvalho de Assis</td>
<td><a href="tel:3729-3829">3729-3829</a></td>
</tr> 
<tr> 
<td>Lucimara</td>
<td><a href="tel:3729-3828">3729-3828</a></td>
</tr> 
<tr> 
<td>Tiago Henrique de Toledo Braz</td>
<td><a href="tel:3729-3833">3729-3833</a></td>
</tr> 
<tr> 
<td>Giulia Quinteiro</td>
<td><a href="tel:3729-3834">3729-3834</a></td>
</tr> 
<tr> 
<td>Wellington Alber Guimaraes</td>
<td><a href="tel:3729-3858">3729-3858</a></td>
</tr> 
<tr> 
<td>Lucas Sementile</td>
<td><a href="tel:3729-3857">3729-3857</a></td>
</tr> 
<tr> 
<td>Wilson Rodrigues da Silva</td>
<td><a href="tel:3729-3823">3729-3823</a></td>
</tr> 
<tr> 
<td>Claudia</td>
<td><a href="tel:3729-3838">3729-3838</a></td>
</tr> 
</table>
</body>
</html>`