export const listaContatos = () => String.raw `
<!doctype html>
<html lang="en">
<head>
  
  <style>
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    font-size: 2.3em;
  }
  
  td,th {
    text-align: center;
    padding: 5px;
  }

  .name {
    font-weight: bold;

  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }

  a:link {
    color: #59864b;
    text-decoration: none;
  }
  
  a:visited {
    color: #59864b;
    text-decoration: none;
  }
  
  a:hover {
    color: #000;
    text-decoration: none;
  }
  
  a:active {
    color: #59864b;
    text-decoration: none;
  }
  </style>

</head>
<body>
<table>
<tr>
  <td colspan="2">Aliff Jimenes Cicon (Assessor do Ver. Claudiney)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99934-6479 ">99934-6479 </a></td>
</tr>
<tr>
  <td colspan="2">Aline Fallaci de Almeida ( Assessora do Ver. Flávio Silva )</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99748-3058 ">99748-3058 </a></td>
</tr>
<tr>
  <td colspan="2">Aline Pereira ( Assessora da Ver. Regina Cioffi )</td>
</tr><tr>
  <td><a href="mailto: "> </a></td>
  <td><a href="tel:99942-4843 ">99942-4843 </a></td>
</tr>
<tr>
  <td colspan="2">Ana Amélia Franco ( Assessora de Imprensa )</td>
</tr><tr>
  <td><a href="mailto:imprensa@pocosdecaldas.mg.leg.br">imprensa@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99905-1127 ">99905-1127 </a></td>
</tr>
<tr>
  <td colspan="2">Ana Paula Lopes ( Auxiliar Legislativo )</td>
</tr><tr>
  <td><a href="mailto:anapaula@pocosdecaldas.mg.leg.br">anapaula@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99710-7886 ">99710-7886 </a></td>
</tr>
<tr>
  <td colspan="2">Antonio Vieira Machado Netto ( Assistente Legislativo )</td>
</tr><tr>
  <td><a href="mailto:antoniovmnetto@pocosdecaldas.mg.leg.br">antoniovmnetto@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99742-5941 ">99742-5941 </a></td>
</tr>
<tr>
  <td colspan="2">Christopher Salviano Pires ( Assessor do Ver. Roberto dos Santos )</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:98836-9306 ">98836-9306 </a></td>
</tr>
<tr>
  <td colspan="2">Cláudia Carmen Ribeiro Franco ( Assessora Ver. Wilson Rodrigues )</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99238-9509 ">99238-9509 </a></td>
</tr>
<tr>
  <td colspan="2">Claudiney Donizetti Marques ( Vereador PSDB )</td>
</tr><tr>
  <td><a href="mailto:claudiney@pocosdecaldas.mg.leg.br">claudiney@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99802-5986 ">99802-5986 </a></td>
</tr>
<tr>
  <td colspan="2">Cleiton Basso Da Ré ( Assistente Legislativo )</td>
</tr><tr>
  <td><a href="mailto:cleiton@pocosdecaldas.mg.leg.br">cleiton@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:3714-2590">3714-2590</a></br><a href="tel:98843-8101 ">98843-8101</a></td>
</tr>
<tr>
  <td colspan="2">Cristiano Sales Medeiros ( Assessor Jurídico )</td>
</tr><tr>
  <td><a href="mailto:cristianomedeiros@pocosdecaldas.mg.leg.br">cristianomedeiros@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99920-8326 ">99920-8326 </a></td>
</tr>
<tr>
  <td colspan="2">Darcy Aparecida Gouveia (Guarda Municipal)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99918-9303 ">99918-9303 </a></td>
</tr>
<tr>
  <td colspan="2">Deivide Fabiano Pereira (Assistente Legislativo)</td>
</tr><tr>
  <td><a href="mailto:deivide@pocosdecaldas.mg.leg.br">deivide@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:3721-0181">3721-0181</a></br><a href="tel:99196-3771">99196-3771</a></td>
</tr>
<tr>
  <td colspan="2">Diney Lenon de Paulo (Vereador PT)</td>
</tr><tr>
  <td><a href="mailto:diney@pocosdecaldas.mg.leg.br">diney@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99955-7301 ">99955-7301 </a></td>
</tr>
<tr>
  <td colspan="2">Douglas Braga Silva (Assessor Técnico de Informática)</td>
</tr><tr>
  <td><a href="mailto:douglas@pocosdecaldas.mg.leg.br">douglas@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98875-5226 ">98875-5226 </a></td>
</tr>
<tr>
  <td colspan="2">Douglas Eduardo de Souza - Dofu (Vereador DEM)</td>
</tr><tr>
  <td><a href="mailto:douglas.dofu@pocosdecaldas.mg.leg.br">douglas.dofu@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98702-1486 ">98702-1486 </a></td>
</tr>
<tr>
  <td colspan="2">Filipe Augusto Caetano Sancho (Assessor Técnico Legislativo)</td>
</tr><tr>
  <td><a href="mailto:filipesancho@pocosdecaldas.mg.leg.br">filipesancho@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99179-2926 ">99179-2926 </a></td>
</tr>
<tr>
  <td colspan="2">Flávio Togni de Lima e Silva (Vereador PSDB)</td>
</tr><tr>
  <td><a href="mailto:flavinho@pocosdecaldas.mg.leg.br">flavinho@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99952-9353 ">99952-9353 </a></td>
</tr>
<tr>
  <td colspan="2">Giulia Maria Teixeira Pamplona Quinteiro (Assessora do Ver. Tiago Braz)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99948-2389 ">99948-2389 </a></td>
</tr>
<tr>
  <td colspan="2">Isadora Prévide Bernardo (Assessora Ver. Douglas Dofu)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:98405-9888 ">98405-9888 </a></td>
</tr>
<tr>
  <td colspan="2">Jailton Bernardes Faria Dias (Segurança)</td>
</tr><tr>
  <td><a href="mailto:jailton@pocosdecaldas.mg.leg.br">jailton@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99119-5688 ">99119-5688 </a></td>
</tr>
<tr>
  <td colspan="2">Karina Monteiro Moraes Oliveira (Assessora Ver. Kleber Silva)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99225-6255 ">99225-6255 </a></td>
</tr>
<tr>
  <td colspan="2">Kleber de Moura Gavião (Assessor Administrativo)</td>
</tr><tr>
  <td><a href="mailto:kleber@pocosdecaldas.mg.leg.br">kleber@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99109-6502 ">99109-6502 </a></td>
</tr>
<tr>
  <td colspan="2">Kleber Gonçalves da Silva (Vereador NOVO)</td>
</tr><tr>
  <td><a href="mailto:klebersilva@pocosdecaldas.mg.leg.br">klebersilva@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99856-8664 ">99856-8664 </a></td>
</tr>
<tr>
  <td colspan="2">Leonardo Ítalo Lindolfo (Assistente Legislativo)</td>
</tr><tr>
  <td><a href="mailto:leonardo@pocosdecaldas.mg.leg.br">leonardo@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99914-2522 ">99914-2522 </a></td>
</tr>
<tr>
  <td colspan="2">Lourdes Galhego (Guarda Municipal)</td>
</tr><tr>
  <td><a href="mailto:lourdesgalhego@hotmail.com">lourdesgalhego@hotmail.com</a></td>
  <td><a href="tel:98836-8835 ">98836-8835 </a></td>
</tr>
<tr>
  <td colspan="2">Lucas Carvalho de Arruda (Vereador REDE)</td>
</tr><tr>
  <td><a href="mailto:lucasarruda@pocosdecaldas.mg.leg.br">lucasarruda@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98810-9745 ">98810-9745 </a></td>
</tr>
<tr>
  <td colspan="2">Lucimara Poloniato (Assessora Ver. Sílvio Véio)</td>
</tr><tr>
  <td><a href="mailto:lu.poloniato@hotmail.com">lu.poloniato@hotmail.com</a></td>
  <td><a href="tel:99126-9315">99126-9315</a></br><a href="tel:3721-7683">3721-7683</a></td>
</tr>
<tr>
  <td colspan="2">Ludmila de Moraes Giacchetta (Assistente Legislativo)</td>
</tr><tr>
  <td><a href="mailto:ludmila@pocosdecaldas.mg.leg.br">ludmila@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99226-9797 ">99226-9797 </a></td>
</tr>
<tr>
  <td colspan="2">Luís Augusto Mapelli Cerri (Chefe de Tesouraria e Recursos Humanos)</td>
</tr><tr>
  <td><a href="mailto:luisaugusto@pocosdecaldas.mg.leg.br">luisaugusto@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:19 98156-3560">19 98156-3560</a></br><a href="tel:19 3646-1362">19 3646-1362</a></td>
</tr>
<tr>
  <td colspan="2">Luzia Teixeira Martins (Vereadora PDT)</td>
</tr><tr>
  <td><a href="mailto:luzia@pocosdecaldas.mg.leg.br">luzia@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98811-9043 ">98811-9043 </a></td>
</tr>
<tr>
  <td colspan="2">Marcelo Heitor da Silva (Vereador PSC)</td>
</tr><tr>
  <td><a href="mailto:marceloheitor@pocosdecaldas.mg.leg.br">marceloheitor@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99161-2422 ">99161-2422 </a></td>
</tr>
<tr>
  <td colspan="2">Marcos Augusto Maciel (Assistente Legislativo)</td>
</tr><tr>
  <td><a href="mailto:marcos@pocosdecaldas.mg.leg.br">marcos@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99226-3768 ">99226-3768 </a></td>
</tr>
<tr>
  <td colspan="2">Marcusé Mesquiari e Silva (Pregoeiro, Chefe de Patrimônio)</td>
</tr><tr>
  <td><a href="mailto:marcuse@pocosdecaldas.mg.leg.br">marcuse@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99135-4399 ">99135-4399 </a></td>
</tr>
<tr>
  <td colspan="2">Maria de Lourdes Rezende de Assis (Auxiliar Legislativo)</td>
</tr><tr>
  <td><a href="mailto:mlourdes@pocosdecaldas.mg.leg.br">mlourdes@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98825-9962 ">98825-9962 </a><a href="tel:3714-1171">3714-1171</a></td>
</tr>
<tr>
  <td colspan="2">Marta Teixeira (Auxiliar de Serviços Gerais)</td>
</tr><tr>
  <td><a href="mailto:marthatexas@hotmail.com">marthatexas@hotmail.com</a></td>
  <td><a href="tel:98702-3862 ">98702-3862 </a></td>
</tr>
<tr>
  <td colspan="2">Matheus Carminatti Angelo (Assistente Legislativo)</td>
</tr><tr>
  <td><a href="mailto:matheus@pocosdecaldas.mg.leg.br">matheus@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99121-2321 ">99121-2321 </a></td>
</tr>
<tr>
  <td colspan="2">Michelle Veloso Betti (Assessora da Ver. Luzia)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:98700-7435 ">98700-7435 </a></td>
</tr>
<tr>
  <td colspan="2">Milena Resende Franco (Assistente Legislativo)</td>
</tr><tr>
  <td><a href="mailto:milena@pocosdecaldas.mg.leg.br">milena@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99183-8668">99183-8668</a></br><a href="tel:3714-2613">3714-2613</a></td>
</tr>
<tr>
  <td colspan="2">Patrícia Vieira (Auxiliar Legislativo)</td>
</tr><tr>
  <td><a href="mailto:patricia@pocosdecaldas.mg.leg.br">patricia@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98825-4825 ">98825-4825 </a></td>
</tr>
<tr>
  <td colspan="2">Paulo Roberto Sebastião da Mata Resende (Contador)</td>
</tr><tr>
  <td><a href="mailto:pauloroberto@pocosdecaldas.mg.leg.br ">pauloroberto@pocosdecaldas.mg.leg.br </a></td>
  <td><a href="tel:99201-4505 ">99201-4505 </a></td>
</tr>
<tr>
  <td colspan="2">Paulo Rubens Muciarone (Assessor do Ver. Ricardo Sabino)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99822-1983 ">99822-1983 </a></td>
</tr>
<tr>
  <td colspan="2">Rafael Alencar Silva (Assessor Ver. Marcelo Heitor)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99192-3418 ">99192-3418 </a></td>
</tr>
<tr>
  <td colspan="2">Rafael Cássio de Faria (Assessor do Ver. Lucas Arruda)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99897-8952 ">99897-8952 </a></td>
</tr>
<tr>
  <td colspan="2">Regina Maria Cioffi Batagini (Vereadora PP)</td>
</tr><tr>
  <td><a href="mailto:reginacioffi@pocosdecaldas.mg.leg.br">reginacioffi@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99192-1100 ">99192-1100 </a></td>
</tr>
<tr>
  <td colspan="2">Ricardo Magno Marcondes (Assessor Financeiro)</td>
</tr><tr>
  <td><a href="mailto:ricardo@pocosdecaldas.mg.leg.br">ricardo@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99107-2030 ">99107-2030 </a></td>
</tr>
<tr>
  <td colspan="2">Ricardo Sabino dos Santos (Vereador PSDB)</td>
</tr><tr>
  <td><a href="mailto:ricardosabino@pocosdecaldas.mg.leg.br">ricardosabino@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98817-7016 ">98817-7016 </a></td>
</tr>
<tr>
  <td colspan="2">Rodrigo Aparecido Galhardi (Assistente Legislativo)</td>
</tr><tr>
  <td><a href="mailto:rodrigo@pocosdecaldas.mg.leg.br">rodrigo@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99126-2790 ">99126-2790 </a></br><a href="tel:3714-5211">3714-5211</a></td>
</tr>
<tr>
  <td colspan="2">Roseane de Cássia Garcia (Auxiliar de Serviços Gerais)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99735-7236 ">99735-7236 </a></td>
</tr>
<tr>
  <td colspan="2">Rosilene Maria de Oliveira (Auxiliar de Serviços Gerais)</td>
</tr><tr>
  <td><a href="mailto:rosilene@pocosdecaldas.mg.leg.br">rosilene@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98802-1906 ">98802-1906 </a></td>
</tr>
<tr>
  <td colspan="2">Rosimeire Loiola da Silva (Assessora do Ver. Diney)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99120-9091 ">99120-9091 </a></td>
</tr>
<tr>
  <td colspan="2">Sebastião Roberto dos Santos (Vereador REPUB)</td>
</tr><tr>
  <td><a href="mailto:robertosantos@pocosdecaldas.mg.leg.br">robertosantos@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99958-0210 ">99958-0210 </a></td>
</tr>
<tr>
  <td colspan="2">Silvio Rogério Carvalho de Assis - Silvio Véio (Vereador MDB)</td>
</tr><tr>
  <td><a href="mailto:silvioveio@pocosdecaldas.mg.leg.br">silvioveio@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99186-3415 ">99186-3415 </a></td>
</tr>
<tr>
  <td colspan="2">Tais Aparecida Ferreira (Diretora da Escola do Legislativo)</td>
</tr><tr>
  <td><a href="mailto:tais@pocosdecaldas.mg.leg.br">tais@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98709-5560 ">98709-5560 </a></td>
</tr>
<tr>
  <td colspan="2">Tiago Henrique Silva de Toledo Braz (Vereador REDE)</td>
</tr><tr>
  <td><a href="mailto:tiagobraz@pocosdecaldas.mg.leg.br">tiagobraz@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:98802-7595 ">98802-7595 </a></td>
</tr>
<tr>
  <td colspan="2">Valdeli Negrini Franco (Auxiliar de Serviços Gerais)</td>
</tr><tr>
  <td><a href="mailto:"></a></td>
  <td><a href="tel:99172-7530 ">99172-7530 </a></td>
</tr>
<tr>
  <td colspan="2">Valdilene Aparecida de Oliveira (Auxiliar de Serviços Gerais)</td>
</tr><tr>
  <td><a href="mailto:valdilene@pocosdecaldas.mg.leg.br">valdilene@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99986-9605">99986-9605</a></br><a href="tel:3713-4393">3713-4393</a></td>
</tr>
<tr>
  <td colspan="2">Vanessa Carlos (Chefe de Expediente)</td>
</tr><tr>
  <td><a href="mailto:vanessa@pocosdecaldas.mg.leg.br">vanessa@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:3722-5395 ">3722-5395 </a></td>
</tr>
<tr>
  <td colspan="2">Wellington Alber Guimarães - Paulista (Vereador DEM)</td>
</tr><tr>
  <td><a href="mailto:paulista@pocosdecaldas.mg.leg.br">paulista@pocosdecaldas.mg.leg.br</a></td>
  <td><a href="tel:99106-5912 ">99106-5912 </a></td>
</tr>
<tr>
  <td colspan="2">Wilson Rodrigues da Silva (Vereador DEM)</td>
</tr><tr>
  <td><a href="mailto:wilson@pocosdecaldas.mg.leg.br ">wilson@pocosdecaldas.mg.leg.br </a></td>
  <td><a href="tel:98701-6919 ">98701-6919 </a></td>
</tr>
</table>
</html>`